package edu.sjsu.android.zoodirectory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MyAdapter(createList(), this);
        recyclerView.setAdapter(mAdapter);
    }

    public List<int[]> createList() {
        final List<int[]> input = new ArrayList<>();
        int[] sloth = {R.string.sloth, R.string.slothDesc, R.drawable.sloth};
        input.add(sloth);
        int[] polarBear = {R.string.polar_bear, R.string.polarBearDesc, R.drawable.polar_bear};
        input.add(polarBear);
        int[] panda = {R.string.panda, R.string.pandaDesc, R.drawable.panda};
        input.add(panda);
        int[] koala = {R.string.koala, R.string.koalaDesc, R.drawable.koala};
        input.add(koala);
        int[] kangaroo = {R.string.kangaroo, R.string.kangarooDesc, R.drawable.kangaroo};
        input.add(kangaroo);
        return input;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.information:
                Intent myIntent = new Intent(this, ZooInfoActivity.class);
                startActivity(myIntent);
                return true;
            case R.id.uninstall:
                 Uri packageURI = Uri.parse("package:" + getApplicationContext().getPackageName());
                 Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
                 startActivity(uninstallIntent);
                 return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}