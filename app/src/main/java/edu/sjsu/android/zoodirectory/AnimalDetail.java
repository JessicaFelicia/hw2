package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AnimalDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_detail);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        int title = extras.getInt("Title");
        int description = extras.getInt("Description");
        int image = extras.getInt("Image");
        ImageView animalPicture = findViewById(R.id.animalPicture);
        animalPicture.setImageResource(image);
        TextView animalTitle = findViewById(R.id.animalTitle);
        animalTitle.setText(title);
        TextView text = findViewById(R.id.animalDesc);
        text.setText(description);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.information:
                Intent myIntent = new Intent(this, ZooInfoActivity.class);
                startActivity(myIntent);
                return true;
            case R.id.uninstall:
                Uri packageURI = Uri.parse("package:" + getApplicationContext().getPackageName());
                Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
                startActivity(uninstallIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
