package edu.sjsu.android.zoodirectory;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<int[]> values;
    private Context context;

    public MyAdapter(List<int[]> myDataset, Context context) {
        values = myDataset;
        this.context = context;
    }

    public void add(int position, int[] item) {
        values.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final int[] data = values.get(position);
        holder.Text.setText(data[0]);
        holder.Image.setImageResource(data[2]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final Intent intent = new Intent(view.getContext(), AnimalDetail.class);
                if (position == getItemCount() - 2) {
                    // Start: AlertDialog (https://developer.android.com/guide/topics/ui/dialogs)
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Cuteness Overload!!");
                    builder.setCancelable(true);
                    builder.setMessage(R.string.warning)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    intent.putExtra("Title", data[0]);
                                    intent.putExtra("Description", data[1]);
                                    intent.putExtra("Image", data[2]);
                                    view.getContext().startActivity(intent);
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    intent.putExtra("Title", data[0]);
                    intent.putExtra("Description", data[1]);
                    intent.putExtra("Image", data[2]);
                    view.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView Text;
        public ImageView Image;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            Text = (TextView) v.findViewById(R.id.firstLine);
            Image = (ImageView) v.findViewById(R.id.icon);
        }
    }
}
